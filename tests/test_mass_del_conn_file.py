#!/usr/bin/env python3
import unittest
import logging
import db_scripts
from types import SimpleNamespace


class TestMassDelConnFile(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = db_scripts.guac_db_connect()
        args = SimpleNamespace(conn_group="CS491",conn_file="../input/test-conns")
        #SETUP DB for test
        db_scripts.mass_add_conn(args,logger)
        db_scripts.mass_del_conn_file(args,logger)
        #CHECK THEY'RE GONE
        sql = "select * from guacamole_connection where connection_name = 'CS491-testuser1'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertEqual([],res)
        sql = "select * from guacamole_connection where connection_name = 'CS491-testuser2'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertEqual([],res)
        sql = "select * from guacamole_connection where connection_name = 'CS491-testuser3'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertEqual([],res)

if __name__ == '__main__':
    unittest.main()
