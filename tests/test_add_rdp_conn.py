#!/usr/bin/env python3
import unittest
import logging
import db_scripts
import db_scripts.guac_db_conn as gdb

class TestAddRDPConn(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()

        db_scripts.add_rdp_conn(conn_name,"1234","CS491",logger)
        sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_name(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = ''
        cursor, guacDB = gdb.guac_db_connect()

        ret=db_scripts.add_rdp_conn(conn_name,"1234","CS491",logger)
        self.assertEqual("NONAME",ret)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_group(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test'
        cursor, guacDB = gdb.guac_db_connect()

        ret=db_scripts.add_rdp_conn(conn_name,"1234","",logger)
        self.assertEqual("NOGROUP",ret)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_no_port(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test'
        cursor, guacDB = gdb.guac_db_connect()

        ret=db_scripts.add_rdp_conn(conn_name,"","CS491",logger)
        self.assertEqual("NOPORT",ret)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

    def test_dup(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = gdb.guac_db_connect()

        db_scripts.add_rdp_conn(conn_name,"1234","CS491",logger)
        db_scripts.add_rdp_conn(conn_name,"1234","CS491",logger)
        sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "DELETE from guacamole_connection where connection_name = '%s'"%conn_name
        cursor.execute(sql)
        guacDB.commit()

if __name__ == '__main__':
    unittest.main()
