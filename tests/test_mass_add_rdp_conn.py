#!/usr/bin/env python3
import unittest
import logging
import db_scripts
from types import SimpleNamespace


class TestMassAddConn(unittest.TestCase):
    def test_normal(self):
        logger = logging.getLogger('ncr-sql')
        conn_name = 'test1'
        cursor, guacDB = db_scripts.guac_db_connect()
        args = SimpleNamespace(conn_group="CS491",conn_file="../input/test-conns")

        db_scripts.mass_add_rdp_conn(args,logger)
        sql = "select * from guacamole_connection where connection_name = 'CS491-testuser1'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "select * from guacamole_connection where connection_name = 'CS491-testuser2'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        sql = "select * from guacamole_connection where connection_name = 'CS491-testuser3'"
        cursor.execute(sql)
        res = cursor.fetchall()
        self.assertIsNotNone(res)
        db_scripts.mass_del_conn_file(args,logger)


if __name__ == '__main__':
    unittest.main()
