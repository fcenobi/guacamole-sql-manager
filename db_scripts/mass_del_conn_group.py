#!/usr/bin/env python3

import mysql.connector
import db_scripts
import db_scripts.guac_db_conn as gdb

def mass_del_conn_group(args=None,logger=None):

    cursor,guacDB = gdb.guac_db_connect()

    #mycursor.execute("SHOW TABLES")
    #get conn id from conn entry
    sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%args.conn_group
    cursor.execute(sql)
    res = cursor.fetchall()
    group_id = res[0][0]

    #choice = input("Are you sure you want to delete all connections in group(%s)?\n(y/n): "%args.conn_group)
    #if choice.lower() != 'y':
    #    logger.info("Aborting deletion of connections in %s(%s)"%(args.conn_group,group_id))
    #    exit(0)

    logger.info("Deleting all connections in %s(%s)"%(args.conn_group,group_id))
    sql = "DELETE from guacamole_connection where parent_id = '%s'"%group_id
    cursor.execute(sql)
    guacDB.commit()

    #cleanup
    cursor.close()
    guacDB.close()

