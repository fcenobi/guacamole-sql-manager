#!/usr/bin/env python3

import mysql.connector
import datetime
import db_scripts.guac_db_conn as gdb
import db_scripts

def add_user(netid,conn_name,logger):
    cursor,guacDB = gdb.guac_db_connect()

    #CREATE NEW ENTITY
    logger.info("Creating Entity(%s)"%netid)
    sql = "INSERT INTO guacamole_entity (name,type) VALUES (%s, %s)"
    val = (netid, 'USER')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
    except:
        logger.error("Entity(%s) failed to be created"%netid)
        return "DUP"

    #GET entity_ID
    sql = "select entity_id from guacamole_entity where name = '%s'"%netid
    try:
        cursor.execute(sql)
    except:
        logger.error("Entity(%s) failed to be found"%netid)
        return
    res = cursor.fetchall()
    entity_id = res[0][0]

    #ADD USER
    logger.info("Creating User(%s)"%netid)
    sql = "INSERT INTO guacamole_user (entity_id,password_hash,password_salt,disabled,expired,password_date) VALUES (%s,%s,%s,%s,%s,%s)"
    val = (entity_id,"S3cur3p@ssw0rd12345","saltyMcSalty",0,0,'2020-01-08 17:20:43')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("User(%s) created"%netid)
    except:
        logger.error("User(%s) failed to be created"%netid)
    cursor.close()
    guacDB.close()
