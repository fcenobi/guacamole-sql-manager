#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb

def del_group(args=None,logger=None):

    cursor,guacDB = gdb.guac_db_connect()

    cursor = guacDB.cursor()
    parentID = None

    if args.conn_group is None:
        logger.error("conn_group missing for ",args.conn_group)
        return

    #get conn id from conn entry
    sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%args.conn_group
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        logger.info("Group(%s) is found"%args.conn_group)
    except:
        logger.error("Group(%s) is missing. Check spelling of conn_group"%args.conn_group)
        return

    logger.info("Deleting Guacamole Connection Group: %s"%args.conn_group)

    sql = "DELETE from guacamole_connection_group where connection_group_name = '%s'"%args.conn_group
    try:
        cursor.execute(sql)
        guacDB.commit()
        logger.info("Connection Group(%s) was deleted"%args.conn_group)
    except:
        logger.error("Connection Group(%s) failed to be deleted"%args.conn_group)
        return
