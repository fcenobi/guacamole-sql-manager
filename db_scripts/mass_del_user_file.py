#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb

def mass_del_user_file(args,logger):
    cursor,guacDB = gdb.guac_db_connect()

    if args.netid_file is None:
        logger.info("Missing netid_file argument")
        exit(1)

    #choice = input("Are you sure you want to delete all users in file(%s)?\n(y/n): "%args.netid_file)
    #if choice.lower() != 'y':
    #    logger.info("Aborting deletion of connections in file(%s)"%args.conn_file)
    #    exit(0)

    netids = []
    try:
        netid_file = open(args.netid_file)
    except:
        logger.info("Error reading netid file")
        exit(1)
    for line in netid_file:
        netids.append(line.strip('\n').split(' '))
    
    netid_file.close()
    logger.info("Done reading netid file")

    for netidLine in netids:
        netid = netidLine[0]
        #get entity id
        sql = "select entity_id from guacamole_entity where name = '%s'"%netid
        cursor.execute(sql)
        res = cursor.fetchall()
        if len(res) == 0:
            logger.info("User %s not found"%netid)
        entity_id = res[0][0]
        logger.info("Deleting User(%s) Id(%s)"%(netid,entity_id))
        #Remove Entity
        sql = "DELETE from guacamole_entity where name = '%s'"%netid
        cursor.execute(sql)
        guacDB.commit()
        logger.info("Deleted User(%s) Id(%s)"%(netid,entity_id))

    #close conns
    cursor.close()
    guacDB.close()
