import mysql.connector
import db_scripts.guac_db_conn as gdb
import db_scripts

def add_user_to_group(netid,conn_group,logger):
    cursor,guacDB = gdb.guac_db_connect()

    cursor = guacDB.cursor()

    if len(netid) == 0:
        logger.info("netid missing")
        return

    #GET entity_ID
    sql = "select entity_id from guacamole_entity where name = '%s'"%netid
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        entity_id = res[0][0]
    except:
        logger.error("Failed to get entity id for user(%s) when trying to add_perm for connection(%s)"%(netid,conn_name))
        return

    #add user to conn_group
    if conn_group is not None:
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%conn_group
        try:
            cursor.execute(sql)
            res = cursor.fetchall()
            group_id = res[0][0]
        except:
            logger.error("Could not locate group(%s).\nPlease check spelling of conn_group")
            exit(1)
        #give access to group
        sql = "INSERT INTO guacamole_connection_group_permission (entity_id,connection_group_id,permission) VALUES (%s, %s, %s)"
        val = (entity_id, group_id,'READ')
        try:
            cursor.execute(sql, val)
            guacDB.commit()
            logger.info("User(%s) given access to Group(%s)"%(netid,conn_group))
        except:
            logger.warning("User(%s) all ready has access to Group(%s)"%(netid,conn_group))
    else:
        logger.error("User(%s) not given access a Group since conn_group missing")

