#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb
import db_scripts

def mass_add_conn(args=None,logger=None):

    if args.conn_group is None:
        logger.error("Missing conn_group arg")
        exit(1)
    if args.conn_file is None:
        logger.error("Missing conn_file arg")
        exit(1)
    cursor,guacDB = gdb.guac_db_connect()

    Parent_Id = None
    conns = []
    Protocol = 'vnc'
    
    try:
        conn_file = open(args.conn_file)
    except:
        logger.error("Error reading connection file")
        exit(1)

    for line in conn_file:
        conns.append(line.strip('\n').split(' '))

    conn_file.close()
    for conn in conns:
        conn_name = conn[0]
        host = conn[1]
        hostport = conn[2]
        #def add_conn(conn_name,hostport,conn_group,logger,Protocol='vnc',vmhost='ncr-0.ncr',read_only=None,max_conns=None):
        db_scripts.add_conn(conn_name,hostport,args.conn_group,logger,vmhost=host)

    #clean up
    cursor.close()
    guacDB.close()
    logger.info("Cleaned up and exited properly")


