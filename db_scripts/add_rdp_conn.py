#!/usr/bin/env python3

import mysql.connector
import db_scripts.guac_db_conn as gdb

def add_rdp_conn(conn_name,hostport,conn_group,logger,Protocol='rdp',vmhost='ncr-0.ncr',read_only='false',max_conns=None):

    cursor,guacDB = gdb.guac_db_connect()

    cursor = guacDB.cursor()

    if len(conn_name) == 0:
        logger.error("Conn_name missing for ",conn_name)
        return "NONAME"
    if len(hostport) == 0:
        logger.error("hostport missing for ",conn_name)
        return "NOPORT"
    if len(conn_group) == 0:
        logger.error("conn_group missing for ",conn_name)
        return "NOGROUP"

    #get conn id from conn entry
    sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        logger.warning("Connection(%s) Already Exists"%conn_name)
        #return
    except:
        logger.info("Connection(%s) is free"%conn_name)

    logger.info("Creating Guacamole Connection: %s"%conn_name)
    #Parent Conn Group
    if conn_group is not None:
        sql = "select connection_group_id from guacamole_connection_group where connection_group_name = '%s'"%conn_group
        cursor.execute(sql)
        Parent_Id = cursor.fetchall()[0][0]
        logger.info("Found group id (%s) for group: %s"%(Parent_Id,conn_group))

    #create conn
    sql = "INSERT INTO guacamole_connection (connection_name, parent_id,protocol,max_connections,failover_only) VALUES (%s,%s,%s,%s,0)"
    val = (conn_name, Parent_Id,Protocol,max_conns)
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Connection(%s) created"%conn_name)
    except:
        logger.error("Connection(%s) failed to be created or might already exist"%conn_name)
        return

    #get conn id from conn entry
    sql = "select connection_id from guacamole_connection where connection_name = '%s'"%conn_name
    try:
        cursor.execute(sql)
        res = cursor.fetchall()
        conn_id = res[0][0]
        logger.info("Connection id(%s) for Connection(%s)"%(conn_id,conn_name))
    except:
        logger.error("Connection(%s) failed to be found"%conn_name)
        return

    #set conn attribs
    logger.info("Adding attributes to connection: %s"%conn_name)
    #set VNC Cursor to remote to hide local cursor
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'cursor','remote')
    try:
        cursor.execute(sql, val)
    except:
        logger.error("Cursor failed to be added to Connection(%s)"%conn_name)
        return
    #set VNC host
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'hostname',vmhost)
    try:
        cursor.execute(sql, val)
        logger.info("Added Host(%s) for Connection(%s)"%(vmhost,conn_name))
    except:
        logger.error("Host(%s) failed to be added to Connection(%s)"%(vmhost,conn_name))
        return
    #set VNC port
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'port',hostport)
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added Port(%s) for Connection(%s)"%(hostport,conn_name))
        logger.info("Done adding attributes to Connection(%s)"%conn_name)
    except:
        logger.error("VNC Port(%s) failed to be added to Connection(%s)"%(hostport,conn_name))
        return
    #set USER
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'username','${GUAC_USERNAME}')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added username for Connection(%s)"%conn_name)
    except:
        logger.error("Failed to add username to Connection(%s)"%conn_name)
        return
    #set password
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'password','${GUAC_PASSWORD}')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added password for Connection(%s)"%conn_name)
    except:
        logger.error("Failed to add password to Connection(%s)"%conn_name)
        return
    #set DOMAIN
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'domain','UNR')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added domain for Connection(%s)"%conn_name)
    except:
        logger.error("Failed to add domain to Connection(%s)"%conn_name)
        return
    #set SECURITY
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'security','any')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added security mode for Connection(%s)"%conn_name)
    except:
        logger.error("Failed to add security mode to Connection(%s)"%conn_name)
        return
    #set IGNORE-CERT
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'ignore-cert','true')
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added ignore cert for Connection(%s)"%conn_name)
    except:
        logger.error("Failed to add ignore cert to Connection(%s)"%conn_name)
        return
    #set READ ONLY
    sql = "INSERT INTO guacamole_connection_parameter (connection_id, parameter_name,parameter_value) VALUES (%s,%s,%s)"
    val = (conn_id, 'read-only',read_only)
    try:
        cursor.execute(sql, val)
        guacDB.commit()
        logger.info("Added READ-ONLY(%s) for Connection(%s)"%(read_only,conn_name))
    except:
        logger.error("Read Only(%s) failed to be set on Connection(%s)"%(read_only,conn_name))
        return

    #DONE! :D
    logger.info("Done adding attributes to Connection(%s)"%conn_name)
    guacDB.commit()
    cursor.close()
    guacDB.close()


